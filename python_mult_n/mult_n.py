from argparse import ArgumentParser
import decimal

def mult(n):
    """
    Multiply all entries in an input file by parameter `n` and write results to an output file.
    """
    out = open('/tmp/output', 'w')
    with open('/tmp/input', 'r') as f:
        # import pdb; pdb.set_trace()
        for line in f:
            try:
                dec = decimal.Decimal(line.strip())
                mul = str(dec*n) + '\n'
                out.write(mul)
            except Exception as e:
                print "Exception on line: ", line, " exception: ", str(e)
                # write any non-number to output file 'as is'
                out.write(line)
    out.close()

def main():
    parser = ArgumentParser(description="File system replication via git")
    parser.add_argument('-v', '--version', action='version', version="0.1")
    parser.add_argument('-f', '--factor', help='factor to multiply by.')
    args = parser.parse_args()
    if not args.factor:
        args.factor = 2
    mult(int(args.factor))

if __name__ == "__main__":
    main()


