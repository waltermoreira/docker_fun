#!/bin/bash

ARG=$1

nf.py $ARG
/nextflow run "${ARG%.*}.nf"
