# Installs the Agave auth services (/clients and /profiles) on top of the python base image.

from jstubbs/python

# Pull in the bitbucket ssh key
RUN mkdir /root/.ssh
ADD autodep /root/.ssh/autodep
RUN echo "IdentityFile /root/.ssh/autodep" >> /etc/ssh/ssh_config
RUN chmod 600 /root/.ssh/autodep

# this line is here to get the bitbucket key in known hosts. Without it, the
# git clone below was failing with error 'Host Verification failed'; see this
# SO entry: http://bit.ly/1t5m3nz
RUN ssh -v -o StrictHostKeyChecking=no git@bitbucket.org

# pull down the latest version of the repo:
RUN yum -y install git
RUN cd /; git clone git@bitbucket.org:jstubbs/agave-ldap.git

# Add dependency libraries: mysql, openldap:
RUN yum -y install mysql-devel openldap-devel

# create the virtualenv
RUN cd /virtualenvs; virtualenv-2.7 -p /usr/local/bin/python2.7 --distribute agave-ldap
RUN source /virtualenvs/agave-ldap/bin/activate && pip install -r /agave-ldap/src/users/requirements.txt

# add the deployment settings:
ADD deployment_settings.py /agave-ldap/src/users/usersApp/deployment_settings.py

# run the dev server
CMD ["/virtualenvs/agave-ldap/bin/python", "/agave-ldap/src/users/manage.py", "runserver", "8000"]
