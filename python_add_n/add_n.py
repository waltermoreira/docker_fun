from argparse import ArgumentParser
import decimal

def increment(n):
    """
    Increment all entries in an input file by parameter `n` and write results to an output file.
    """
    out = open('/data/output.txt', 'w')
    with open('/data/input.txt', 'r') as f:
        # import pdb; pdb.set_trace()
        for line in f:
            try:
                dec = decimal.Decimal(line.strip())
                inc = str(dec+n) + '\n'
                out.write(inc)
            except Exception as e:
                print "Exception on line: ", line, " exception: ", str(e)
                # write any non-number to output file 'as is'
                out.write(line)
    out.close()

def main():
    parser = ArgumentParser(description="File system replication via git")
    parser.add_argument('-v', '--version', action='version', version="0.1")
    parser.add_argument('-i', '--increment', help='amount to increment.')
    args = parser.parse_args()
    if not args.increment:
        args.increment = 1
    increment(int(args.increment))

if __name__ == "__main__":
    main()


