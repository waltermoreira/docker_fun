import decimal



def sum():
    """
    Sum all entries in an input file and write results to an output file.
    """
    with open('/data/in.txt', 'r') as f:
        sum = 0
        for line in f:
            try:
                dec = decimal.Decimal(line.strip())
                sum = dec + sum
            except Exception as e:
                print "Exception on line: ", line, " exception: ", str(e)
                # ignore any non-numbers in input
    with open('/data/out.txt', 'w') as out:
        out.write(str(sum) + '\n')


if __name__ == '__main__':
    sum()